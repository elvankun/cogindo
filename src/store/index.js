/**
 *  @project Front Office
 *  @author Elvan Diano
 *  @email elvandiano@gmail.com
 *  @description BaseEntity vuex to handling user login
 */

import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './auth.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth
  }
})

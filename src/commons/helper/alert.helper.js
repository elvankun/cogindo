/**
 *  @project Energy Management System
 *  @author Elvan Diano
 *  @email elvandiano@gmail.com
 *  @description Custom sweetalert
 */

import swall from 'sweetalert'

const alert = {
  confirmation (callback, confirmationText = '') {
    swall({
      icon: 'warning',
      title: 'Perhatian',
      text: confirmationText === '' ? 'Apakah anda yakin ingin menghapus data ini?' : confirmationText,
      buttons: ['Tidak', 'Iya']
    }).then(willDelete => {
      if (willDelete) {
        if (callback) callback()
      }
    })
  },

  success (successText = '', callback) {
    swall({
      icon: 'success',
      title: 'Berhasil',
      text: successText === '' ? '' : successText,
      button: true
    }).then(result => {
      if (result) {
        if (callback) callback()
      }
    })
  },

  failed (failText = '', callback) {
    swall({
      icon: 'error',
      title: 'Gagal',
      text: failText === '' ? 'Terjadi Kesalahan, Silahkan Coba Kembali' : failText,
      button: true
    }).then(result => {
      if (result) {
        if (callback) callback()
      }
    })
  },

  custom (status, successText = '', callback) {
    swall({
      icon: status === 200 ? 'success' : 'warning',
      title: status === 200 ? 'Berhasil' : 'warning',
      text: successText === '' ? '' : successText,
      button: true
    }).then(result => {
      if (result) {
        if (callback) callback()
      }
    })
  }
}

export default alert

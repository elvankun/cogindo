import Vue from 'vue'
import VueRouter from 'vue-router'

import DefaultContainer from '../layout/DefaultContainer'
import DefaultDetailContainer from '../layout/DefaultDetailContainer'

import Beranda from '../views/components/beranda/ViewBeranda'
import Product from '../views/components/product/ViewProduct'

import Login from '../views/components/auth/Login'
import Register from '../views/components/auth/Registrer'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: 'beranda',
    component: DefaultDetailContainer,
    children: [
      {
        path: 'beranda',
        name: 'Beranda',
        component: Beranda
      }
    ]
  },
  {
    path: '/look',
    redirect: '/product',
    component: DefaultContainer,
    children: [
      {
        path: '/product',
        name: 'Product Detail',
        component: Product
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
